# About the content
- The idea behind this repository is to help sharing a common knowledge of
  worldwide maritime cyber incidents (such as ports, ships, offshore, MRE...).
- This knowledge can then be used:
  - to increase the awareness of the maritime sector on incidents
  - hopefully, to contribute to the reduction of new incidents
  - to help researchers, students and maritime and cybersecurity professionals work on the subject.
- The data is provided AS IS, meaning its quality and comprehensiveness depends
  on the source of the information. However, we do our best to keep it accurate and up
  to date.
- The subject does not quote the victim name. This is done on purpose, as our goal
  is not to point at them. However, the references will of course quote them.
- Disclosed incidents are just a probably small part of what really exists,
  so be careful when derivating hypothetis, conclusions, research subjects,
  or diagrams from this source.
- The data is shared as a CSV file, to help you build information from it with
  the format you wish.

# Citing the content
To use this content for research paper or other kind of publication (media, social networks),
please quote it:
"Advanced Data of Maritime cyber Incidents ReleAsed for Litterature, ADMIRAL repository, retrieved from https://gitlab.com/m-cert/admiral"

# Contributing to the repository

By contributing to the ADMIRAL database, you accept and agree to the the LICENSE
and the following guidelines.
You reserve all right, title, and interest in and to Your Contributions.
 
You are welcome to add issues and pull requests to improve the database.
Your Pull Request will be reviewed by one of our devs/volunteers and you will be asked to reformat it if needed. We don't bite and we will try to be as flexible as possible, so don't get intimidated and feel free to ask for support!

When suggesting for the addition of new or past incidents:
- please make sure they are not already listed ;-)
- please use the same CSV format :

|YYYY_indexnumber|Year|Month|Day|Country|Target|Incident detail|Type|Reference| where :
- "YYYY_indexnumber" in the year in 4 digits, where indexnumber is the current MAX value for the index of the year in question and add 1
- "Year" is the Year in 4 digits when the incident occured (if known), or the date of the first disclosure report published
- "Month" is the Month in 2 digits when the incident occured (if known), or the date ofthe first disclosure report published
- "Day" is the Day in 2 digits when the incident occured (if known), or the the first disclosure report published
  If unsure or unknown, insert "----" or "--"
- "Country" is the country is the country ISO code (2 digits) where the incident happened. In case of a global event, add the ISO code of the
  country of the headquarters company
- "Target" is the maritime domain target to be chosen between: Defence, Offshore, Shipowner, Indutry, Port, Organization, Shipyard, Logistics,
  Education, MRE
- "Incident detail" gives a summary of the incident (1 or 2 lines)
- "Type" gives and idea about the source threat of the incident (Political, Cybercrime, Terrorism, Espionage, Undisclosed). As the precise
  imputation is sometimes hard to determine, the use of Undisclosed should prevail
- "Reference": if possible, add the initial first reference reporting the incident. Other references are possible if they add updates, technical
  details, etc.

Please stick as close as possible to the guidelines. That way we ensure quality guides and easy to merge requests.

## License
See [LICENSE](LICENSE).

## Merge Request title
Try to be as descriptive as you can in your Merge Request title, such as:
* [YYYY_indexnumber] Incident
And just add the current CSV file with your update(s)
