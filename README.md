# About ADMIRAL

This project aims to centralize disclosed cybersecurity incidents in the maritime sector for research activites and other projects in the maritime cybersecurity sector.

# Why the name ADMIRAL?

The acronym stands for **A**dvanced **D**atabase of **M**aritime cyber **I**ncidents **R**ele**a**sed for **L**itterature (although we know it's not precisely speaking a database ;-) ).

# Sources and information protection

When possible and relevant, the source of the information is given.
Information is strictly derived from OSINT sources.
Project is released with the <span style="color: white;" style="background-color:black">TLP:CLEAR</span>.

# Why ADMIRAL?

- The acronym itself was chosen because an Admiral is a highly-ranked officer within Navies, with a lot of wisdom, knowledge and experience, and willing to share those with his sailors and officers. Well, we humbly hope this database is a good way to share our knowledge to enhance cyber awareness in the maritime sector.
- This file was at first invented and compiled by Olivier JACQ on his own blog on maritime cybersecurity ( https://www.cybermaretique.fr ) and he agreed to reformat it and release through the M-CERT on this repository. We wish to thank him for this.
- As Oliver claims "I've worked for more than 20 years now on maritime cybersecurity and I've always been searching for this kind of data for the awareness sessions I give. As there was no real central information, I started to compile a file and released the information on my website. I'm very happy of the M-CERT creation, and I'm happy to share this data if it can be of help to anyone, for awareness, press or research studies."

Please read the CONTRIBUTING and LICENSE file for more information about the contribution process and the proper sourcing for this data. Using, copying or derivating work from this data without respecting the directives givent in the LICENSE file is prohibited.

**Please, do not use it to disclose private incidents, but contact your CERT organization, such as the Maritime Computer Emergency Response Team (M-CERT, https://www.m-cert.fr ).**
